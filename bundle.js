'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var admin = require('firebase-admin');
var admin__default = _interopDefault(admin);
var express = _interopDefault(require('express'));
var cors = _interopDefault(require('cors'));
var path = _interopDefault(require('path'));
var bodyParser = _interopDefault(require('body-parser'));
var responseTime = _interopDefault(require('response-time'));

const router = express.Router();

router.get('/', (req, res) => {
  res.sendStatus(200);
});

router.post('/create', (req, res) => {
  const db = admin__default.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .set({
      test: 'test'
    })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error setting doc', err);
    });
});

router.get('/read', (req, res) => {
  const db = admin__default.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .get()
    .then((doc) => {
      res.send(doc.id);
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error reading doc', err);
    });
});

router.post('/update', (req, res) => {
  const db = admin__default.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .update({ test: 'test1' })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error updating doc', err);
    });
});

router.delete('/delete', (req, res) => {
  const db = admin__default.firestore();

  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .update({ test: admin.FieldValue.delete() })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error getting doc', err);
    });
});

router.get('/error', (req, res) => {
  const db = admin__default.firestore();

  const docRef = db.collection('test1')
    .doc('test');
  docRef.get()
    .then((doc) => {
      if (!doc.exists) {
        res.send("Error doc doesn't exist");
      } else {
        res.send(doc.data());
      }
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error getting doc', err);
    });
});

const createGame = async (timestamp, rules) => {
  const db = admin__default.firestore();
  const collection = db.collection('DrinkingGames');
  let response;

  await collection.add({
    timestamp,
    rules
  }).then((document) => {
    response = document.id;
  }).catch((err) => {
    response = err;
  });
  return response;
};

const createNewGame = async (req, res) => {
  const { body } = req;
  const { timestamp, rules } = body;
  if (!rules) {
    return res.sendStatus(400);
  }
  Object.keys(rules).map((rule) => {
    const { drinkingRule, alcoholName, alcoholDenomination } = rules[rule];
    if (!drinkingRule && !alcoholName && !alcoholDenomination) {
      delete rules[rule];
    }
    return null;
  });

  return res.json(await createGame(timestamp, rules));
};

const router$1 = express.Router();

router$1.post('/', createNewGame);

const getGameList = async (id) => {
  const db = admin__default.firestore();

  const collection = db.collection('DrinkingGames');
  let response;

  await collection
    .doc(id)
    .get()
    .then((doc) => {
      if (doc.exists) {
        response = doc.data();
      } else {
        response = 'error';
      }
    })
    .catch((e) => {
      response = e;
    });
  return response;
};

const getList = async (req, res) => {
  const { params } = req;
  const { id } = params;


  const returnPayload = await getGameList(id);
  if (returnPayload === 'error') {
    return res.sendStatus(400);
  }
  return res.json(returnPayload);
};

const router$2 = express.Router();

router$2.get('/:id', getList);

/* eslint-disable import/order */

admin__default.initializeApp({
  credential: admin__default.credential.applicationDefault(),
  databaseURL: 'https://takeashot-5a1fa.firebaseio.com'
});


const app = express();
app.use(bodyParser.json());
const corsOptions = {
  exposedHeaders: 'x-response-time'
};
app.use(cors(corsOptions));
app.use(responseTime());

app.use('/test', router);
app.use('/create', router$1);
app.use('/view', router$2);

const port = process.env.PORT || 8081;

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'takeashot_fe/build')));
  // Handle React routing, return all requests to React app
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'takeashot_fe/build', 'index.html'));
  });
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${port}!`);
});

module.exports = app;
