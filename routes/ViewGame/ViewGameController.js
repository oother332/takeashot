import { getGameList } from './ViewGameService';

export const getList = async (req, res) => {
  const { params } = req;
  const { id } = params;


  const returnPayload = await getGameList(id);
  if (returnPayload === 'error') {
    return res.sendStatus(400);
  }
  return res.json(returnPayload);
};
