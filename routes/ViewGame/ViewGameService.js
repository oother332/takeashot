import admin from 'firebase-admin';

export const getGameList = async (id) => {
  const db = admin.firestore();

  const collection = db.collection('DrinkingGames');
  let response;

  await collection
    .doc(id)
    .get()
    .then((doc) => {
      if (doc.exists) {
        response = doc.data();
      } else {
        response = 'error';
      }
    })
    .catch((e) => {
      response = e;
    });
  return response;
};
