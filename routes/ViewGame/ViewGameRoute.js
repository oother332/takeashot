import express from 'express';
import { getList } from './ViewGameController';

const router = express.Router();

router.get('/:id', getList);

export default router;
