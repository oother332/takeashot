import express from 'express';
import admin, { FieldValue } from 'firebase-admin';

const router = express.Router();

router.get('/', (req, res) => {
  res.sendStatus(200);
});

router.post('/create', (req, res) => {
  const db = admin.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .set({
      test: 'test'
    })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error setting doc', err);
    });
});

router.get('/read', (req, res) => {
  const db = admin.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .get()
    .then((doc) => {
      res.send(doc.id);
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error reading doc', err);
    });
});

router.post('/update', (req, res) => {
  const db = admin.firestore();
  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .update({ test: 'test1' })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error updating doc', err);
    });
});

router.delete('/delete', (req, res) => {
  const db = admin.firestore();

  db.collection('test')
    .doc('m7qauY8jRUsS5msSbTD8')
    .update({ test: FieldValue.delete() })
    .then(() => {
      res.send('Success');
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error getting doc', err);
    });
});

router.get('/error', (req, res) => {
  const db = admin.firestore();

  const docRef = db.collection('test1')
    .doc('test');
  docRef.get()
    .then((doc) => {
      if (!doc.exists) {
        res.send("Error doc doesn't exist");
      } else {
        res.send(doc.data());
      }
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error getting doc', err);
    });
});

export default router;
