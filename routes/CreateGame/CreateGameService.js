import admin from 'firebase-admin';

export const createGame = async (timestamp, rules) => {
  const db = admin.firestore();
  const collection = db.collection('DrinkingGames');
  let response;
  await collection.add({
    timestamp,
    rules
  }).then((document) => {
    response = document.id;
  }).catch((err) => {
    response = err;
  });
  return response;
};

export default createGame;
