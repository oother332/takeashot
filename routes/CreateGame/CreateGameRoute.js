import express from 'express';
import { createNewGame } from './CreateGameController';

const router = express.Router();

router.post('/', createNewGame);

export default router;
