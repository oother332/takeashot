import { createGame } from './CreateGameService';

export const createNewGame = async (req, res) => {
  const { body } = req;
  const { timestamp, rules } = body;
  if (!rules) {
    return res.sendStatus(400);
  }
  Object.keys(rules).map((rule) => {
    const { drinkingRule, alcoholName, alcoholDenomination } = rules[rule];
    if (!drinkingRule && !alcoholName && !alcoholDenomination) {
      delete rules[rule];
    }
    return null;
  });

  return res.json(await createGame(timestamp, rules));
};
