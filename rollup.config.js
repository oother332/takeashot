import json from '@rollup/plugin-json';

module.exports = {
  input: 'app.js',
  output: {
    file: 'bundle.js',
    format: 'cjs'
  },
  plugins: [json()]
};
