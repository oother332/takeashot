import sharedReducer, { SHOW_SNACKBAR, CLOSE_SNACKBAR } from '../../redux/ducks/shared';
import testStoreState from '../testStoreState';


describe('create reducer', () => {
  it('should return the initial state', () => {
    expect(sharedReducer(undefined, {})).toEqual({ ...testStoreState.shared });
  });

  it('should handle SHOW_SNACKBAR', () => {
    const message = 'Success';
    const expectedReducerState = {
      ...testStoreState.shared,
      snackBarMessage: message,
      showSnackBar: true
    };
    const newReducerState = sharedReducer(undefined, {
      type: SHOW_SNACKBAR,
      snackBarMessage: message
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });
  it('should handle CLOSE_SNACKBAR', () => {
    const expectedReducerState = {
      ...testStoreState.shared,
      showSnackBar: false
    };
    const newReducerState = sharedReducer(undefined, {
      type: CLOSE_SNACKBAR,
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });
});
