import gameReducer, {
  UPDATE_DRINKING_RULE, CHECK_ADD_NEW_DRINKING_RULE, SUCCESS_CREATE_NEW_GAME,
  SUCCESS_FETCH_CURRENT_GAME, FAIL_FETCH_CURRENT_GAME, CHECK_DELETE_DRINKING_RULE, SET_FETCHING
} from '../../redux/ducks/game';
import { defaultRule } from '../../constants/constants';
import testStoreState from '../testStoreState';

describe('game reducer', () => {
  it('should return the initial state', () => {
    expect(gameReducer(undefined, {})).toEqual({ ...testStoreState.game });
  });

  it('should handle UPDATE_DRINKING_RULE', () => {
    let expectedReducerState = {
      ...testStoreState.game,
      rules: {
        0: {
          drinkingRule: 'Maggie cries',
          alcoholName: '',
          alcoholDenomination: '',
          valid: false
        }
      }
    };
    let newReducerState = gameReducer(undefined, {
      type: UPDATE_DRINKING_RULE,
      updatedRuleIndex: 0,
      updatedFieldName: 'drinkingRule',
      updatedFieldValue: 'Maggie cries'
    });
    expect(newReducerState).toEqual(expectedReducerState);

    expectedReducerState = {
      ...testStoreState.game,
      rules: {
        0: {
          drinkingRule: '',
          alcoholName: 'tequila',
          alcoholDenomination: '',
          valid: false
        }
      }
    };
    newReducerState = gameReducer(undefined, {
      type: UPDATE_DRINKING_RULE,
      updatedRuleIndex: 0,
      updatedFieldName: 'alcoholName',
      updatedFieldValue: 'tequila'
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle CHECK_ADD_NEW_DRINKING_RULE', () => {
    const expectedReducerState = {
      ...testStoreState.game,
      rules: {
        0: defaultRule,
        1: defaultRule
      },
      latestRuleIndex: 1
    };
    const newReducerState = gameReducer(undefined, {
      type: CHECK_ADD_NEW_DRINKING_RULE,
      index: 0,
    });

    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle CHECK_DELETE_DRINKING_RULE', () => {
    const initialState = {
      ...testStoreState.game,
      rules: {
        0: defaultRule,
        1: defaultRule
      },
      latestRuleIndex: 1
    };

    const expectedReducerState = {
      ...testStoreState.game,
      rules: {
        1: defaultRule
      },
      latestRuleIndex: 1
    };

    const newReducerState = gameReducer(initialState, {
      type: CHECK_DELETE_DRINKING_RULE,
      index: 0,
    });

    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle SUCCESS_CREATE_NEW_GAME', () => {
    const expectedReducerState = { ...testStoreState.game, gameId: 'a' };
    const newReducerState = gameReducer(undefined, {
      type: SUCCESS_CREATE_NEW_GAME,
      gameId: 'a'
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle SUCCESS_CREATE_NEW_GAME', () => {
    const expectedReducerState = { ...testStoreState.game, gameId: 'a' };

    const newReducerState = gameReducer(undefined, {
      type: SUCCESS_CREATE_NEW_GAME,
      gameId: 'a'
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle SUCCESS_FETCH_CURRENT_GAME', () => {
    const expectedReducerState = {
      ...testStoreState.game,
      rules: {
        0: defaultRule,
        1: defaultRule
      },
      numRules: 2,
      latestRuleIndex: 1,
      gameInvalid: false
    };
    const newReducerState = gameReducer(undefined, {
      type: SUCCESS_FETCH_CURRENT_GAME,
      rules: {
        0: defaultRule,
        1: defaultRule
      }
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle FAIL_FETCH_CURRENT_GAME', () => {
    const expectedReducerState = {
      ...testStoreState.game,
      rules: 'error'
    };
    const newReducerState = gameReducer(undefined, {
      type: FAIL_FETCH_CURRENT_GAME,
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });

  it('should handle SET_FETCHING', () => {
    const expectedReducerState = {
      ...testStoreState.game,
      rules: true
    };
    const newReducerState = gameReducer(undefined, {
      type: SET_FETCHING,
    });
    expect(newReducerState).toEqual(expectedReducerState);
  });
});
