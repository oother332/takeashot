import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { fetchGameSaga, requestFetchGame } from '../../sagas/fetchGameSaga';
import { defaultRule } from '../../constants/constants';
import { successFetchCurrentGame, failFetchCurrentGame } from '../../redux/ducks/game';
import { showSnackBar } from '../../redux/ducks/shared';

describe('fetch game saga', () => {
  const requestData = 'anId';

  it('should work on success', () => {
    const response = {
      status: 200,
      data: {
        rules: {
          0: defaultRule
        }
      }
    };
    return expectSaga(fetchGameSaga, requestFetchGame)
      .provide([[matchers.call.fn(requestFetchGame, requestData), response]])
      .put(successFetchCurrentGame(response.data.rules))
      .run();
  });

  it('should handle error response', () => {
    const error = {
      response: {
        status: 404
      }
    };
    const snackBarMessage = 'Failure Retrieving Game';
    return expectSaga(fetchGameSaga, requestFetchGame)
      .provide([[matchers.call.fn(requestFetchGame, requestData), throwError(error)]])
      .put(showSnackBar(snackBarMessage))
      .put(failFetchCurrentGame())
      .run();
  });

  it('should handle internal server error', () => {
    const error = {
      response: {
        status: 500
      }
    };
    const snackBarMessage = 'Server Error';
    return expectSaga(fetchGameSaga, requestFetchGame)
      .provide([[matchers.call.fn(requestFetchGame, requestData), throwError(error)]])
      .put(showSnackBar(snackBarMessage))
      .put(failFetchCurrentGame())
      .run();
  });
});
