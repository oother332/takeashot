import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { createGameSaga, requestCreateGame } from '../../sagas/createGameSaga';
import { SUCCESS_CREATE_NEW_GAME } from '../../redux/ducks/game';
import { SHOW_SNACKBAR, showSnackBar } from '../../redux/ducks/shared';
import { defaultRule } from '../../constants/constants';


describe('create game saga', () => {
  const currentTime = Date.now();
  const requestData = {
    timestamp: currentTime,
    rules: {
      0: {
        defaultRule
      }
    }
  };
  it('should work on success', () => {
    const response = {
      status: 200,
      data: 'a'
    };
    return expectSaga(createGameSaga, requestCreateGame)
      .provide([[matchers.call.fn(requestCreateGame, requestData), response]])
      .put({ type: SUCCESS_CREATE_NEW_GAME, gameId: response.data })
      .put({ type: SHOW_SNACKBAR, snackBarMessage: 'Success' })
      .run();
  });
  it('should handle error response', () => {
    const error = {
      response: {
        status: 404
      }
    };

    const snackBarMessage = 'Failure Creating Game';
    return expectSaga(createGameSaga, requestCreateGame)
      .provide([[matchers.call.fn(requestCreateGame, requestData), throwError(error)]])
      .put(showSnackBar(snackBarMessage))
      .run();
  });
  it('should handle internal server error', () => {
    const error = {
      response: {
        status: 500
      }
    };
    const snackBarMessage = 'Server Error';
    return expectSaga(createGameSaga, requestCreateGame)
      .provide([[matchers.call.fn(requestCreateGame, requestData), throwError(error)]])
      .put(showSnackBar(snackBarMessage))
      .run();
  });
});
