import { defaultRule } from '../constants/constants';

const testStoreState = {
  game: {
    gameId: undefined,
    numRules: 0,
    rules: {
      0: defaultRule
    },
    latestRuleIndex: 0,
    gameInvalid: true
  },
  shared: {
    snackBarMessage: '',
    showSnackBar: false
  }
};

export default testStoreState;
