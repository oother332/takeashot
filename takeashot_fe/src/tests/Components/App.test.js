import React from 'react';
import { mount } from 'enzyme';
import App from '../../Components/App';
import TestComponentWrapper from '../TestComponentWrapper';


describe('App', () => {
  it('should render', () => {
    const wrapper = mount(
      <TestComponentWrapper>
        <App />
      </TestComponentWrapper>
    );
    expect(wrapper).toBeDefined();
  });
});
