import React from 'react';
import { mount } from 'enzyme';
import InputDrinkingRule from '../../../Components/DrinkingRule/InputDrinkingRule';
import TestComponentWrapper from '../../TestComponentWrapper';
import { defaultRule } from '../../../constants/constants';


describe('Game Rule with Inputs', () => {
  const getWrapper = (rule = defaultRule) => mount(
    <TestComponentWrapper>
      <InputDrinkingRule rule={rule} />
    </TestComponentWrapper>
  );

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });
  it('should have three inputs', () => {
    const wrapper = getWrapper();
    const inputs = wrapper.find('input');
    expect(inputs).toHaveLength(3);
  });
  it('should match input values to props passed in', () => {
    const testAlcohol = 'vodka';
    const testDrinkingRule = 'Homer says d\'oh!';
    const testDenomination = 'One shot';
    const rulePassedIn = {
      alcoholName: testAlcohol,
      alcoholDenomination: testDenomination,
      drinkingRule: testDrinkingRule
    };
    const wrapper = getWrapper(rulePassedIn);

    const denominationDataTest = '[data-test="alcohol-denomination-input"]';
    const nameDataTest = '[data-test="alcohol-name-input"]';
    const ruleDataTest = '[data-test="drinking-rule-input"]';

    const denomination = wrapper.find(denominationDataTest).hostNodes().childAt(0).prop('value');
    const name = wrapper.find(nameDataTest).hostNodes().childAt(0).prop('value');
    const rule = wrapper.find(ruleDataTest).hostNodes().childAt(0).prop('value');

    expect(rule).toBe(testDrinkingRule);
    expect(name).toBe(testAlcohol);
    expect(denomination).toBe(testDenomination);
  });
});
