import React from 'react';
import { mount } from 'enzyme';
import TestComponentWrapper from '../../TestComponentWrapper';
import DrinkingRule from '../../../Components/DrinkingRule/DrinkingRule';

describe('Drinking Rule', () => {
  const getWrapper = (rule = {}) => mount(
    <TestComponentWrapper>
      <DrinkingRule rule={rule} />
    </TestComponentWrapper>
  );
  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });
  it('should match input values to props passed in', () => {
    const testAlcohol = 'vodka';
    const testDrinkingRule = 'Homer says d\'oh!';
    const testDenomination = 'One shot';
    const rule = {
      alcoholName: testAlcohol,
      alcoholDenomination: testDenomination,
      drinkingRule: testDrinkingRule
    };
    const expectedText = `Take ${testDenomination} of ${testAlcohol} when ${testDrinkingRule}`;
    const dataTest = '[data-test="rule-text"]';

    const wrapper = getWrapper(rule);
    const ruleText = wrapper.find(dataTest).text();

    expect(ruleText).toBe(expectedText);
  });
});
