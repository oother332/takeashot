import React from 'react';
import { mount } from 'enzyme';
import Home from '../../Components/Home/Home';
import TestComponentWrapper from '../TestComponentWrapper';
import * as messages from '../../messages/en.json';


describe('Home', () => {
  const getWrapper = () => {
    return mount(
      <TestComponentWrapper>
        <Home />
      </TestComponentWrapper>
    );
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });

  it('should have a Start button', () => {
    const wrapper = getWrapper();
    const button = wrapper.find('[data-test="start-button"]').hostNodes();
    expect(button).toHaveLength(1);
    const text = button.text();
    expect(text).toEqual(messages.start_new_game_button);
  });
});
