import React from 'react';
import { mount } from 'enzyme';
import RuleList from '../../Components/RuleList';
import TestComponentWrapper from '../TestComponentWrapper';
import * as messages from '../../messages/en.json';
import { defaultRule } from '../../constants/constants';
import InputDrinkingRule from '../../Components/DrinkingRule/InputDrinkingRule';
import DrinkingRule from '../../Components/DrinkingRule/DrinkingRule';

describe('Rule List', () => {
  const getWrapper = (type = 'create') => {
    const rules = { 0: defaultRule, 1: defaultRule, 2: defaultRule };
    const reducerOverride = {
      game: {
        rules
      }
    };
    const screenName = type;
    const dataTest = `${type}-game-drinking-rule`;
    const generateRule = (id, rule) => ((type !== 'view') ? (
      <InputDrinkingRule key={id} id={id} rule={rule} data-test={dataTest} />
    ) : (
      <DrinkingRule key={id} id={id} rule={rule} data-test={dataTest} />
    ));

    return mount(
      <TestComponentWrapper reducerOverride={reducerOverride}>
        <RuleList screenName={screenName} generateRule={generateRule} />
      </TestComponentWrapper>
    );
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });

  it('should generate input rules for create', () => {
    const wrapper = getWrapper('create');
    const dataTest = '[data-test="create-game-drinking-rule"]';

    const rules = wrapper.find(dataTest);
    expect(rules).toHaveLength(3);

    const rule = rules.first();
    expect(rule.type()).toBe(InputDrinkingRule);
  });

  it('should generate input rules for edit', () => {
    const wrapper = getWrapper('edit');
    const dataTest = '[data-test="edit-game-drinking-rule"]';

    const rules = wrapper.find(dataTest);
    expect(rules).toHaveLength(3);

    const rule = rules.first();
    expect(rule.type()).toBe(InputDrinkingRule);
  });

  it('should generate input rules for view', () => {
    const wrapper = getWrapper('view');
    const dataTest = '[data-test="view-game-drinking-rule"]';

    const rules = wrapper.find(dataTest);
    expect(rules).toHaveLength(3);

    const rule = rules.first();
    expect(rule.type()).toBe(DrinkingRule);
  });

  it('should have an create button', () => {
    const wrapper = getWrapper('create');
    const dataTest = '[data-test="create-game-button"]';
    const button = wrapper.find(dataTest).hostNodes();
    const text = button.text();
    expect(button).toHaveLength(1);
    expect(text).toEqual(messages.create_game_button);
  });

  it('should have an edit button', () => {
    const wrapper = getWrapper('edit');
    const dataTest = '[data-test="edit-game-button"]';
    const button = wrapper.find(dataTest).hostNodes();
    const text = button.text();
    expect(button).toHaveLength(1);
    expect(text).toEqual(messages.edit_game_button);
  });

  it('should have an view button', () => {
    const wrapper = getWrapper('view');
    const dataTest = '[data-test="view-game-button"]';
    const button = wrapper.find(dataTest).hostNodes();
    const text = button.text();
    expect(button).toHaveLength(1);
    expect(text).toEqual(messages.view_game_button);
  });
});
