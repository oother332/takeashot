import React from 'react';
import { mount } from 'enzyme';
import CreateGame from '../../../Components/CreateGame/CreateGame';
import TestComponentWrapper from '../../TestComponentWrapper';
import { defaultRule } from '../../../constants/constants';
import * as messages from '../../../messages/en.json';

describe('Create Game', () => {
  const getWrapper = () => {
    const rules = { 0: defaultRule, 1: defaultRule, 2: defaultRule };
    const reducerOverride = {
      game: {
        rules
      }
    };
    return (mount(
      <TestComponentWrapper reducerOverride={reducerOverride}>
        <CreateGame />
      </TestComponentWrapper>
    ));
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });

  it('should have all rules', () => {
    const wrapper = getWrapper();
    const dataTest = '[data-test="create-game-drinking-rule"]';
    const rules = wrapper.find(dataTest);
    expect(rules).toHaveLength(3);
  });

  it('should have a Create Game button', () => {
    const wrapper = getWrapper();
    const dataTest = '[data-test="create-game-button"]';
    const button = wrapper.find(dataTest).hostNodes();
    const text = button.text();
    expect(button).toHaveLength(1);
    expect(text).toEqual(messages.create_game_button);
  });
});
