import React from 'react';
import { mount } from 'enzyme';
import EditGame from '../../../Components/EditGame/EditGame';
import TestComponentWrapper from '../../TestComponentWrapper';
import { defaultRule } from '../../../constants/constants';


describe('EditGame', () => {
  const getWrapper = () => {
    const match = { params: { listId: 'a' } };
    const rules = { 0: defaultRule, 1: defaultRule };
    const reducerOverride = {
      game: {
        rules
      }
    };
    return mount(
      <TestComponentWrapper reducerOverride={reducerOverride}>
        <EditGame match={match} />
      </TestComponentWrapper>
    );
  };

  it('should render', () => {
    const wrapper = getWrapper();

    expect(wrapper).toBeDefined();
  });

  it('should render two rules', () => {
    const wrapper = getWrapper();
    const dataTest = '[data-test="edit-game-drinking-rule"]';
    const rules = wrapper.find(dataTest);
    expect(rules).toHaveLength(2);
  });
});
