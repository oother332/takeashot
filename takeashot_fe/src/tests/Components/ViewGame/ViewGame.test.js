import React from 'react';
import { mount } from 'enzyme';
import ViewGame from '../../../Components/ViewGame/ViewGame';
import TestComponentWrapper from '../../TestComponentWrapper';
import { defaultRule } from '../../../constants/constants';
import * as messages from '../../../messages/en.json';


describe('ViewGame', () => {
  const getWrapper = () => {
    const match = { params: { listId: 'a' } };
    const rules = { 0: defaultRule, 1: defaultRule, 2: defaultRule };
    const reducerOverride = {
      game: {
        rules
      }
    };
    return (mount(
      <TestComponentWrapper reducerOverride={reducerOverride}>
        <ViewGame match={match} />
      </TestComponentWrapper>
    ));
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });
  it('should display some rules', () => {
    const dataTest = '[data-test="view-game-drinking-rule"]';
    const dummyRules = { 0: defaultRule, 1: defaultRule, 2: defaultRule };

    const wrapper = getWrapper(dummyRules);
    const numRules = wrapper.find(dataTest);

    expect(numRules).toHaveLength(3);
  });
  it('should have an Edit Game button', () => {
    const dataTest = '[data-test="view-game-button"]';
    const wrapper = getWrapper();
    const button = wrapper.find(dataTest).hostNodes();
    const text = button.text();

    expect(button).toHaveLength(1);
    expect(text).toEqual(messages.view_game_button);
  });
});
