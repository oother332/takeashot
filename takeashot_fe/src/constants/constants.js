import light from '../themes/light';
import dark from '../themes/dark';

const BACKEND_PROD_URL = 'https://takeashot.herokuapp.com';
const BACKEND_DEV_URL = 'http://localhost:8081';

export const backendURL = process.env.NODE_ENV === 'production' ? BACKEND_PROD_URL : BACKEND_DEV_URL;

export const availableThemes = { light, dark };

export const defaultRule = {
  alcoholDenomination: '',
  alcoholName: '',
  drinkingRule: '',
  valid: false
};

export const textInputProps = {
  style: {
    textAlign: 'center'
  }
};

export const validateRule = (rule, updatedFieldName, updatedFieldValue) => {
  const updatedRule = {
    ...rule,
    [updatedFieldName]: updatedFieldValue
  };
  const { alcoholDenomination, alcoholName, drinkingRule } = updatedRule;
  updatedRule.valid = !(!alcoholDenomination || !alcoholName || !drinkingRule);
  return updatedRule;
};

export const checkAllRulesValid = (rules, latestRuleIndex) => {
  let currentRulesInvalid = false;
  Object.keys(rules).map((ruleId) => {
    const intRuleId = parseInt(ruleId, 10);
    const rule = rules[ruleId];
    if ((!rule.valid && intRuleId !== latestRuleIndex) || Object.keys(rules).length <= 1) {
      currentRulesInvalid = true;
    }
    return null;
  });
  return currentRulesInvalid;
};

export const mockRules = {
  0: {
    alcoholDenomination: 'two shots',
    alcoholName: 'hennessey',
    drinkingRule: 'they drink'
  },
  1: {
    alcoholDenomination: 'a swig',
    alcoholName: 'beer',
    drinkingRule: 'they eat'
  }
};
