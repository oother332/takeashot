export const SHOW_SNACKBAR = 'tas/shared/SHOW_SNACKBAR';
export const CLOSE_SNACKBAR = 'tas/shared/CLOSE_SNACKBAR';

const initialState = {
  snackBarMessage: '',
  showSnackBar: false
};

export default (state = initialState, action) => {
  const { snackBarMessage } = action;
  switch (action.type) {
    case SHOW_SNACKBAR:
      return {
        ...state,
        snackBarMessage,
        showSnackBar: true
      };
    case CLOSE_SNACKBAR:
      return {
        ...state,
        showSnackBar: false
      };
    default:
      return state;
  }
};

export const showSnackBar = snackBarMessage => ({
  type: SHOW_SNACKBAR,
  snackBarMessage
});

export const closeSnackbar = () => ({
  type: CLOSE_SNACKBAR
});
