const SWITCH_THEME = 'tas/settings/SWITCH_THEME';

const initialState = {
  theme: 'light',
  backendTestResponse: undefined,
  backendTestTime: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SWITCH_THEME:
      const { theme } = state;
      return { ...state, theme: theme === 'light' ? 'dark' : 'light' };
    default:
      return state;
  }
};

export const switchTheme = () => ({
  type: SWITCH_THEME
});
