import { defaultRule, validateRule, checkAllRulesValid } from '../../constants/constants';

export const UPDATE_DRINKING_RULE = 'tas/game/UPDATE_RULE';
export const CHECK_ADD_NEW_DRINKING_RULE = 'tas/game/CHECK_ADD_NEW_RULE';
export const CHECK_DELETE_DRINKING_RULE = 'tas/game/DELETE';
export const SET_VALID = 'tas/game/SET_VALID';

export const CREATE_NEW_GAME = 'tas/game/CREATE';
export const SUCCESS_CREATE_NEW_GAME = 'tas/game/SUCCESS_CREATE_NEW_GAME';
export const FAIL_CREATE_NEW_GAME = 'tas/game/FAIL_CREATE_NEW_GAME';

export const FETCH_CURRENT_GAME = 'tas/game/FETCH_CURRENT_GAME';
export const SET_FETCHING = 'tas/game/SET_FETCHING';
export const SUCCESS_FETCH_CURRENT_GAME = 'tas/game/SUCCESS_FETCH_CURRENT_GAME';
export const FAIL_FETCH_CURRENT_GAME = 'tas/game/FAIL_FETCH_CURRENT_GAME';


const initialState = {
  gameId: undefined,
  numRules: 0,
  rules: {
    0: defaultRule
  },
  latestRuleIndex: 0,
  gameInvalid: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_DRINKING_RULE:
      const { rules } = state;
      const { updatedFieldName, updatedFieldValue, updatedRuleIndex } = action;
      const validatedRule = validateRule(
        rules[updatedRuleIndex], updatedFieldName, updatedFieldValue
      );
      return {
        ...state,
        rules: {
          ...rules,
          [updatedRuleIndex]: validatedRule
        }
      };
    case CHECK_ADD_NEW_DRINKING_RULE:
      const { latestRuleIndex } = state;
      return latestRuleIndex === action.index
        ? {
          ...state,
          latestRuleIndex: latestRuleIndex + 1,
          rules: { ...state.rules, [latestRuleIndex + 1]: defaultRule }
        } : state;
    case CHECK_DELETE_DRINKING_RULE: {
      const { index } = action;
      const { latestRuleIndex, rules } = state;
      const { [index]: val, ...rulesWithRuleRemoved } = rules;
      return latestRuleIndex !== index
        ? {
          ...state,
          rules: rulesWithRuleRemoved
        } : state;
    }
    case SET_VALID: {
      const { rules, latestRuleIndex } = state;
      const gameInvalid = checkAllRulesValid(rules, latestRuleIndex);
      return {
        ...state,
        gameInvalid
      };
    }
    case SUCCESS_CREATE_NEW_GAME:
      const { gameId } = action;
      return {
        ...state,
        gameId
      };
    case SUCCESS_FETCH_CURRENT_GAME: {
      const { rules, gameId } = action;
      const numRules = Object.keys(rules).length;
      const latestRuleIndex = numRules - 1;
      return {
        ...state,
        gameId,
        numRules,
        latestRuleIndex,
        rules,
        gameInvalid: false
      };
    }
    case FAIL_FETCH_CURRENT_GAME:
      return {
        ...state,
        rules: 'error'
      };
    case SET_FETCHING:
      return {
        ...state,
        rules: true
      };
    default:
      return state;
  }
};

export const updateDrinkingRule = (updatedRuleIndex, updatedFieldName, updatedFieldValue) => ({
  type: UPDATE_DRINKING_RULE,
  updatedRuleIndex,
  updatedFieldName,
  updatedFieldValue
});

export const checkDeleteDrinkingRule = index => ({
  type: CHECK_DELETE_DRINKING_RULE,
  index
});

export const checkAddNewDrinkingRule = index => ({
  type: CHECK_ADD_NEW_DRINKING_RULE,
  index
});

export const setRulesValid = () => ({
  type: SET_VALID,
});

export const createNewGame = rules => ({
  type: CREATE_NEW_GAME,
  rules
});

export const successCreateNewGame = gameId => ({
  type: SUCCESS_CREATE_NEW_GAME,
  gameId
});

export const fetchCurrentGame = id => ({
  type: FETCH_CURRENT_GAME,
  id
});

export const setFetching = () => ({
  type: SET_FETCHING
});

export const successFetchCurrentGame = (rules, gameId) => ({
  type: SUCCESS_FETCH_CURRENT_GAME,
  rules,
  gameId
});

export const failFetchCurrentGame = () => ({
  type: FAIL_FETCH_CURRENT_GAME
});
