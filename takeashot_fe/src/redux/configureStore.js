import { combineReducers, createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import settingsReducer from './ducks/settings';
import gameReducer, { UPDATE_DRINKING_RULE, CHECK_ADD_NEW_DRINKING_RULE } from './ducks/game';
import sharedReducer from './ducks/shared';
import { watcherSaga } from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger({
  predicate: (getState, action) => action.type !== UPDATE_DRINKING_RULE
  && action.type !== CHECK_ADD_NEW_DRINKING_RULE
});

let middleware = [sagaMiddleware];
if (process.env.NODE_ENV !== 'production') {
  middleware = [...middleware, loggerMiddleware];
}

const reducer = combineReducers({
  game: gameReducer,
  settings: settingsReducer,
  shared: sharedReducer
});

const store = createStore(
  reducer,
  {},
  applyMiddleware(...middleware)
);

sagaMiddleware.run(watcherSaga);

export default store;
