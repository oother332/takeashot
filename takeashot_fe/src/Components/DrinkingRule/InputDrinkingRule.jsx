import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  shape, number, string
} from 'prop-types';
import Input from '@material-ui/core/Input';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core';
import { updateDrinkingRule, checkAddNewDrinkingRule, checkDeleteDrinkingRule } from '../../redux/ducks/game';
import { textInputProps } from '../../constants/constants';

const useStyles = makeStyles(() => ({
  rules: {
    padding: '15px'
  }
}));

const InputDrinkingRule = (props) => {
  const classes = useStyles(props);
  const { rule, id, latestRuleIndex } = props;
  const {
    drinkingRule, alcoholName, alcoholDenomination
  } = rule;
  const intId = parseInt(id, 10);
  const dispatch = useDispatch();

  const handleInputChange = (id, e) => {
    const { target } = e;
    const { name, value } = target;
    dispatch(updateDrinkingRule(id, name, value));
    dispatch(checkAddNewDrinkingRule(id));
  };

  useEffect(() => {
    if (!drinkingRule && !alcoholDenomination && !alcoholName) {
      dispatch(checkDeleteDrinkingRule(id));
    }
  });

  return (
    <>
      <Grid container className={classes.rules} direction="row" alignItems="center" justify="center">
        <Typography>
          <FormattedMessage id="take_text" />
        </Typography>
        <Input
          name="alcoholDenomination"
          inputProps={textInputProps}
          error={alcoholDenomination.length === 0 && latestRuleIndex !== intId}
          placeholder="a shot"
          value={alcoholDenomination}
          onChange={(e) => { handleInputChange(id, e); }}
          data-test="alcohol-denomination-input"
        />
        <Typography>
          <FormattedMessage id="of_text" />
        </Typography>
        <Input
          name="alcoholName"
          inputProps={textInputProps}
          error={alcoholName.length === 0 && latestRuleIndex !== intId}
          placeholder="vodka"
          value={alcoholName}
          onChange={(e) => { handleInputChange(id, e); }}
          data-test="alcohol-name-input"
        />
        <Typography>
          <FormattedMessage id="when_text" />
        </Typography>
        <Input
          name="drinkingRule"
          inputProps={textInputProps}
          error={drinkingRule.length === 0 && latestRuleIndex !== intId}
          placeholder="Kenny dies."
          value={drinkingRule}
          onChange={(e) => { handleInputChange(id, e); }}
          data-test="drinking-rule-input"
        />
      </Grid>
    </>
  );
};

InputDrinkingRule.propTypes = {
  rule: shape({
    drinkingRule: string,
    alcoholName: string,
    alcoholDenomination: string
  }),
  id: number,
  latestRuleIndex: number
};

export default InputDrinkingRule;
