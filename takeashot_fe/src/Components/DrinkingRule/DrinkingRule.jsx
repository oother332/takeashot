import React from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import {
  shape, string, oneOfType, bool
} from 'prop-types';

const useStyles = makeStyles(() => ({
  rule: {
    padding: '15px'
  }
}));

const DrinkingRule = (props) => {
  const classes = useStyles(props);
  const { rule } = props;
  const { drinkingRule, alcoholName, alcoholDenomination } = rule;

  return (
    <>
      <Typography variant="h5" className={classes.rule}>
        <FormattedMessage
          id="rule_message"
          values={{ drinkingRule, alcoholName, alcoholDenomination }}
          data-test="rule-text"
        />
      </Typography>
      <Divider />
    </>
  );
};

DrinkingRule.propTypes = {
  rule: oneOfType([
    shape({
      drinkingRule: string,
      alcohol: string
    }),
    bool,
    string
  ])
};


export default DrinkingRule;
