import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { shape, string } from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import DrinkingRule from '../DrinkingRule/DrinkingRule';
import { fetchCurrentGame, checkAddNewDrinkingRule } from '../../redux/ducks/game';
import RuleList from '../RuleList';

const ViewGame = (props) => {
  const { match } = props;
  const { params } = match;
  const { gameId } = params;
  const rules = useSelector(state => state.game.rules);
  const dispatch = useDispatch();
  const history = useHistory();


  useEffect(() => {
    dispatch(fetchCurrentGame(gameId));
  }, []);

  useEffect(() => {
    if (rules === 'error') {
      history.push('/notfound');
    }
  }, [rules]);

  const handleEditOnClick = () => {
    const latestRuleIndex = Object.keys(rules).length - 1;
    dispatch(checkAddNewDrinkingRule(latestRuleIndex));
    history.push(`/list/${gameId}/edit`);
  };

  const generateRule = (id, rule) => (
    <DrinkingRule key={id} id={id} rule={rule} data-test="view-game-drinking-rule" />
  );

  return (
    <>
      {rules === true ? (
        <CircularProgress />
      ) : (
        <RuleList
          generateRule={generateRule}
          screenName="view"
          handleButtonOnClick={handleEditOnClick}
        />
      )}

    </>
  );
};

ViewGame.propTypes = {
  match: shape({
    params: shape({
      gameId: string
    })
  })
};

export default ViewGame;
