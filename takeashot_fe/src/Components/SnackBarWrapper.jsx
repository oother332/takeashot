import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { closeSnackbar } from '../redux/ducks/shared';

const SnackBar = () => {
  const dispatch = useDispatch();
  const message = useSelector(state => state.shared.snackBarMessage);
  const showSnackBar = useSelector(state => state.shared.showSnackBar);

  const onClickClose = () => {
    dispatch(closeSnackbar());
  };

  return (
    <>
      <Snackbar
        open={showSnackBar}
        message={message}
        autoHideDuration={6000}
        onClose={onClickClose}
        action={[
          <IconButton key={message} onClick={onClickClose}>
            <CloseIcon />
          </IconButton>
        ]}
      />
    </>
  );
};

export default SnackBar;
