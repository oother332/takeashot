import React from 'react';
import { useHistory } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';

const Home = () => {
  const history = useHistory();

  return (
    <>
      <Button
        color="primary"
        variant="contained"
        data-test="start-button"
        onClick={() => history.push('/create')}
      >
        <FormattedMessage id="start_new_game_button" />
      </Button>
    </>
  );
};

export default Home;
