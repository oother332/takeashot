import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import {
  AppBar, Toolbar, Typography, IconButton, Grid, Link
} from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';
import HeaderMenu from './HeaderMenu';

const Header = () => {
  const [anchorEl, setAnchorEl] = useState(null);

  return (
    <AppBar position="static">
      <Toolbar>
        <Grid
          container
          alignItems="center"
          justify="center"
        >
          <Grid item xs={1} />
          <Grid item xs={5}>
            <Link color="inherit" href="/">
              <Typography variant="h5">
                <FormattedMessage id="header_title" />
              </Typography>
            </Link>
          </Grid>
          <Grid item xs={5} container justify="flex-end">
            <IconButton onClick={e => setAnchorEl(e.currentTarget)}>
              <MoreVert />
            </IconButton>
            <HeaderMenu anchorEl={anchorEl} setAnchorEl={setAnchorEl} />
          </Grid>
          <Grid item xs={1} />
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
