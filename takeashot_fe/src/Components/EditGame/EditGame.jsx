import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { shape, string } from 'prop-types';
import RuleList from '../RuleList';
import InputDrinkingRule from '../DrinkingRule/InputDrinkingRule';
import { createNewGame } from '../../redux/ducks/game';

const EditGame = (props) => {
  const { match } = props;
  const { params } = match;
  const { gameId } = params;

  const history = useHistory();
  const dispatch = useDispatch();

  const id = useSelector(state => state.game.gameId);
  const rules = useSelector(state => state.game.rules);
  const latestRuleIndex = useSelector(state => state.game.latestRuleIndex);

  useEffect(() => {
    if (gameId !== id) {
      history.push(`/list/${id}`);
    }
  }, [id]);

  const generateRule = (id, rule) => (
    <InputDrinkingRule key={id} id={id} rule={rule} latestRuleIndex={latestRuleIndex} data-test="edit-game-drinking-rule" />
  );

  const handleEditOnClick = () => {
    dispatch(createNewGame(rules));
  };

  return (
    <>
      <RuleList
        generateRule={generateRule}
        screenName="edit"
        handleButtonOnClick={handleEditOnClick}
      />
    </>
  );
};

EditGame.propTypes = {
  match: shape({
    params: shape({
      gameId: string
    })
  })
};

export default EditGame;
