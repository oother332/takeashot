import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import Home from './Home/Home';
import ViewGame from './ViewGame/ViewGame';
import CreateGame from './CreateGame/CreateGame';
import EditGame from './EditGame/EditGame';
import NotFound from './NotFound';
import SnackBar from './SnackBarWrapper';


const useStyles = makeStyles(() => ({
  root: {
    minHeight: '100vh',
    background: 'silver'
  },
  header: {
    width: '100%'
  },
  footer: {
    marginTop: 'auto',
    padding: '10px'
  }
}));

const App = (props) => {
  const classes = useStyles(props);

  return (
    <>
      <Grid container direction="column" className={classes.root}>
        <Grid item className={classes.header}>
          <Header />
        </Grid>
        <Grid container item align="center" justify="center" className={classes.content}>
          <Grid item xs={2} />
          <Grid item xs={8}>
            <Switch>
              <Route exact path="/" render={props => <Home {...props} />} />
              <Route exact path="/create" render={props => <CreateGame {...props} />} />
              <Route exact path="/list/:gameId" render={props => <ViewGame {...props} />} />
              <Route exact path="/list/:gameId/edit" render={props => <EditGame {...props} />} />
              <Route render={() => <NotFound />} />
            </Switch>
          </Grid>
          <Grid item xs={2} />
        </Grid>
        <div className={classes.footer}>
          <Grid item>
            <SnackBar />
            <Footer />
          </Grid>
        </div>
      </Grid>
    </>
  );
};


export default App;
