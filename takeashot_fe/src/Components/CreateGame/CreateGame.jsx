import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import RuleList from '../RuleList';
import InputDrinkingRule from '../DrinkingRule/InputDrinkingRule';
import { createNewGame } from '../../redux/ducks/game';

const CreateGame = () => {
  const rules = useSelector(state => state.game.rules);
  const gameId = useSelector(state => state.game.gameId);
  const latestRuleIndex = useSelector(state => state.game.latestRuleIndex);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (gameId) {
      history.push(`/list/${gameId}`);
    }
  }, [gameId]);

  const generateRule = (id, rule) => (
    <InputDrinkingRule key={id} id={id} rule={rule} latestRuleIndex={latestRuleIndex} data-test="create-game-drinking-rule" />
  );

  const handleCreateOnClick = () => {
    dispatch(createNewGame(rules));
  };

  return (
    <>
      <RuleList
        generateRule={generateRule}
        screenName="create"
        handleButtonOnClick={handleCreateOnClick}
      />
    </>
  );
};

export default CreateGame;
