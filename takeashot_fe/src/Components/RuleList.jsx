import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/';
import { func, string } from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { setRulesValid } from '../redux/ducks/game';

const useStyles = makeStyles(() => ({
  paper: {
    background: 'silver'
  }
}));

const RuleList = (props) => {
  const {
    generateRule,
    screenName,
    handleButtonOnClick
  } = props;
  const classes = useStyles(props);
  const dispatch = useDispatch();
  const rules = useSelector(state => state.game.rules);
  const gameInvalid = useSelector(state => state.game.gameInvalid);

  useEffect(() => {
    if (screenName !== 'view') {
      dispatch(setRulesValid());
    }
  }, [rules]);

  return (
    <Paper elevation={3}>
      <Grid container className={classes.papers}>
        <Grid container item xs={12}>
          <Grid item xs={1} />
          <Grid item xs={10}>
            {Object.keys(rules).map(ruleId => generateRule(parseInt(ruleId, 10), rules[ruleId]))}
          </Grid>
          <Grid item xs={1} />
        </Grid>
        <Grid item xs={12}>
          <Button
            disabled={gameInvalid}
            variant="contained"
            color="primary"
            data-test={`${screenName}-game-button`}
            onClick={() => handleButtonOnClick()}
            fullWidth
          >
            <FormattedMessage id={`${screenName}_game_button`} />
          </Button>
        </Grid>
      </Grid>
    </Paper>
  );
};

RuleList.propTypes = {
  handleButtonOnClick: func,
  generateRule: func,
  screenName: string
};

export default RuleList;
