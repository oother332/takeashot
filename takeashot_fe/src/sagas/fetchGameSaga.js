import { put, call } from 'redux-saga/effects';
import axios from 'axios';
import { showSnackBar } from '../redux/ducks/shared';
import { successFetchCurrentGame, failFetchCurrentGame, setFetching } from '../redux/ducks/game';
import { backendURL } from '../constants/constants';

// function that makes the api request and returns a Promise for response
export function requestFetchGame(payload) {
  return axios.get(
    `${backendURL}/view/${payload}`
  );
}

// worker saga: makes the api call when watcher saga sees the action
export function* fetchGameSaga(action) {
  yield put(setFetching());
  try {
    const { id } = action;
    const response = yield call(requestFetchGame, id);
    const { status, data } = response;
    const { rules } = data;
    if (status === 200) {
      yield put(successFetchCurrentGame(rules, id));
    }
  } catch (error) {
    const { response } = error;
    const { status } = response;
    if (status >= 400 && status < 500) {
      yield put(showSnackBar('Failure Retrieving Game'));
      yield put(failFetchCurrentGame());
    } else {
      yield put(showSnackBar('Server Error'));
      yield put(failFetchCurrentGame());
    }
  }
}
