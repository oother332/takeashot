import { takeLatest } from 'redux-saga/effects';
import { CREATE_NEW_GAME, FETCH_CURRENT_GAME } from '../redux/ducks/game';
import { createGameSaga } from './createGameSaga';
import { fetchGameSaga } from './fetchGameSaga';
// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* watcherSaga() {
  yield takeLatest(CREATE_NEW_GAME, createGameSaga);
  yield takeLatest(FETCH_CURRENT_GAME, fetchGameSaga);
}
