import { put, call } from 'redux-saga/effects';
import axios from 'axios';
import { showSnackBar } from '../redux/ducks/shared';
import { successCreateNewGame } from '../redux/ducks/game';
import { backendURL } from '../constants/constants';

// function that makes the api request and returns a Promise for response
export function requestCreateGame(payload) {
  return axios.post(
    `${backendURL}/create`,
    payload
  );
}

// worker saga: makes the api call when watcher saga sees the action
export function* createGameSaga(action) {
  try {
    const { rules } = action;
    const currentTime = Date.now();
    const requestData = {
      timestamp: currentTime,
      rules
    };

    const response = yield call(requestCreateGame, requestData);
    const { data, status } = response;
    if (status === 200) {
      yield put(successCreateNewGame(data));
      yield put(showSnackBar('Success'));
    }
  } catch (error) {
    const { response } = error;
    const { status } = response;
    if (status >= 400 && status < 500) {
      yield put(showSnackBar('Failure Creating Game'));
    } else {
      yield put(showSnackBar('Server Error'));
    }
  }
}
