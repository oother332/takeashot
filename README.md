## Server code for Take A Shot

To start:

1. clone repo
2. npm install
3. npm start

Also, make sure the frontend boilerplate is running for the backend work.

### Current Stack

Express (JS)
Firebase
body-parser
cors
