/* eslint-disable import/order */
/* eslint-disable import/first */
import admin from 'firebase-admin';

if (process.env.NODE_ENV === 'production') {
  admin.initializeApp({
    credential: admin.credential.cert(JSON.parse(process.env.GOOGLE_APPLICATION_CREDENTIALS)),
    databaseURL: 'https://takeashot-5a1fa.firebaseio.com'
  });
} else {
  admin.initializeApp({
    credential: admin.credential.cert('./.keys/serviceAccountKey.json'),
    databaseURL: 'https://takeashot-5a1fa.firebaseio.com'
  });
}

import express from 'express';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import responseTime from 'response-time';
import firebaseTest from './routes/firebaseTest';
import CreateGameRoute from './routes/CreateGame/CreateGameRoute';
import ViewGameRoute from './routes/ViewGame/ViewGameRoute';


const app = express();
app.use(bodyParser.json());
const corsOptions = {
  exposedHeaders: 'x-response-time'
};
app.use(cors(corsOptions));
app.use(responseTime());

app.use('/test', firebaseTest);
app.use('/create', CreateGameRoute);
app.use('/view', ViewGameRoute);

const port = process.env.PORT || 8081;

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'takeashot_fe/build')));
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'takeashot_fe/build', 'index.html'));
  });
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Listening on port ${port}!`);
});


export default app;
