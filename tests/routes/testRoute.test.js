import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../app';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('test route', () => {
  it('should return status 200', (done) => {
    chai.request(app)
      .get('/test')
      .end((err, res) => {
        res.status.should.be.equal(200);
        done();
      });
  });
});
