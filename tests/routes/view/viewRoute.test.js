import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../../app';
import { mockViewUrl } from '../../mockData';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('view route', () => {
  it('should return status 200', (done) => {
    chai.request(app)
      .get(`/view/${mockViewUrl}`)
      .end((err, res) => {
        res.status.should.be.equal(200);
        done();
      });
  });

  it('should handle no id', (done) => {
    chai.request(app)
      .get('/view')
      .end((err, res) => {
        res.status.should.be.equal(404);
        done();
      });
  });
});
