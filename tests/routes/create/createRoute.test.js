import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../../app';
import { mockCreateBody } from '../../mockData';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('create route route', () => {
  it('should return status 200', (done) => {
    chai.request(app)
      .post('/create')
      .send(mockCreateBody)
      .end((err, res) => {
        res.status.should.be.equal(200);
        done();
      });
  });
  it('should error on bad data', (done) => {
    chai.request(app)
      .post('/create')
      .end((err, res) => {
        res.status.should.be.equal(400);
        done();
      });
  });
});
