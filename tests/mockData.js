export const mockCreateBody = {
  timestamp: '1578692946',
  rules: {
    0: {
      alcoholDenomination: 'a shot',
      alcoholName: 'vodka',
      drinkingRule: 'homer says doh',
      valid: true
    }
  }
};

export const mockViewUrl = 'bIX7HP6vFGcDAhVShwjn';
